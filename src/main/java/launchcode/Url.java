package launchcode;

public class Url {

    private String baseUrl;
    private String protocol;
    private String domain;
    private String path;


    public Url(String url ){
        this.baseUrl=url;
    }

    public static String getProtocol(String baseUrl) {
        if (baseUrl.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be empty");
        }
            int i = baseUrl.indexOf(":");
            return baseUrl.substring(0, i).toString();
    }

    public static String getDomain(String baseUrl){
        if (baseUrl.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be empty");
        }
        int i = baseUrl.indexOf("://");
        String newUrl = baseUrl.substring(i + 3, baseUrl.length());
        int findSlash = newUrl.indexOf("/");
        return newUrl.substring(0, findSlash).toString();
    }

    public static String getPath(String baseUrl) {
        if (baseUrl.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be empty");
        }
        int i = baseUrl.indexOf("://");
        String newUrl = baseUrl.substring(i + 3, baseUrl.length());
        int findSlash = newUrl.indexOf("/");
        return newUrl.substring(findSlash, newUrl.length()).toString();
    }

}
