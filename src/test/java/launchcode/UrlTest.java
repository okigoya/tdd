package launchcode;


import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;

public class UrlTest {

    String baseurl;
    @Before
    public void setup()   {
        baseurl = "https://launchcode.org/learn";
    }


    //test get protocol returns http, https, ftp or file
    @Test
    public void testGetProtocol() {
       assertEquals("https", Url.getProtocol(baseurl));
    }

    //test get domain returns domain only letters, numbers, .,-,_
    @Test
    public void testGetDomain()  {
        assertEquals("launchcode.org", Url.getDomain(baseurl));
    }

    //test get path returns path
    @Test
    public void testGetPath()  {
        assertEquals("/learn", Url.getPath(baseurl)) ;
    }

    //build url string from protocol, domain & path
    @Test
    public void testBuildUrlString(){

    }






}
